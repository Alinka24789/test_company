<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->delete();
        
        DB::table('positions')->insert([
            ['name' => 'Генeральный директор'],
            ['name' => 'Директор'],
            ['name' => 'Главный инженер'],
            ['name' => 'Начальник участка'],
            ['name' => 'Заместитель начальника участка'],
            ['name' => 'Мастер'],
            ['name' => 'Рабочий']
        ]);
    }
}
