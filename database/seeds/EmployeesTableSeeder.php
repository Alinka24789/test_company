<?php

use DB as SDB;
use Illuminate\Database\Seeder;
use App\Position;
use App\Employee;

class EmployeesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        SDB::table('employees')->delete();

        $positions = Position::all();

        $count = [1, 8, 8, 40, 40, 200, 703];

        foreach ($positions as $key => $position) {

            $this->generate_employee($key, $position, $count[$key]);
        }
    }

    public function generate_employee($level, $position, $count) {

        $parents = Employee::where('level', '=', ($level - 1))->get();

        if (count($parents) == 0) {

            $new_employee = Employee::new_employee(null, $position->id, $level);
            SDB::table('employees')->insert($new_employee);
        } else {

            $new_employees = [];

            for ($i = 1; $i <= $count; $i++) {

                $head = $parents->random();

                $new_employee = Employee::new_employee($head->id, $position->id, $level);

                array_push($new_employees, $new_employee);
            }

            SDB::table('employees')->insert($new_employees);
        }
    }

}
