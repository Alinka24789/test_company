<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'fio',
        'position_id',
        'hire_date',
        'head',
        'level'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function leader() {
        
        return $this->belongsTo('App\Employee', 'head', 'id')->orderBy('fio', 'ASC');
        
    }
    
    public function child() {
        
        return $this->hasMany('App\Employee', 'head', 'id')->orderBy('fio', 'ASC');
    }
    
    public function position() {
        
        return $this->belongsTo('App\Position', 'position_id', 'id');
    }
    
    protected function new_employee($head_id, $position_id, $level) {
        
        return [
            'fio' => $this->generateFIO(),
            'position_id' => $position_id,
            'hire_date' => $this->generateDate(),
            'salary' => rand(10000, 100000),
            'head' => $head_id,
            'level' => $level
        ];
        
    }
    
    protected function generateFIO() {
        
        $surnames = ['Смирнов', 'Иванов', 'Кузнецов', 'Соколов', 'Попов', 'Лебедев', 'Козлов', 'Новиков', 'Морозов', 'Петров', 'Волков',
            'Соловьёв', 'Васильев', 'Зайцев', 'Павлов', 'Семёнов', 'Голубев', 'Виноградов', 'Богданов', 'Воробьёв', 'Фёдоров',
            'Михайлов', 'Беляев', 'Тарасов', 'Белов', 'Комаров', 'Орлов', 'Киселёв', 'Макаров', 'Андреев', 'Ковалёв', 'Ильин', 'Гусев', 'Титов',
            'Кузьмин', 'Кудрявцев', 'Баранов', 'Куликов', 'Алексеев', 'Степанов', 'Яковлев', 'Сорокин', 'Сергеев', 'Романов', 'Захаров', 'Борисов', 'Королёв', 'Герасимов', 'Пономарёв',
            'Григорьев'];

        $names = ['Абрам','Аваз','Августин','Авраам','Агап','Агапит','Агафон',
            'Адам','Адриан','Азамат','Азат','Айдар','Айрат',
            'Акакий','Аким','Богдан','Борис','Борислав','Бронислав','Булат',
            'Давид','Давлат','Дамир','Дана','Даниил','Данислав','Данияр','Дарий',
            'Евгений','Евдоким','Евсей','Евстахий','Егор','Елисей','Емельян','Еремей','Ефим','Ефрем'];
        
        $patronymics=['Ааронович','Абрамович','Августович','Авдеевич','Аверьянович','Адамович','Адрианович','Аксёнович','Александрович','Алексеевич','Анатольевич','Андреевич','Анисимович','Антипович','Антонович','Ануфриевич','Арсенович','Давидович','Давыдович','Даниилович','Данилович','Демидович','Демьянович','Денисович','Димитриевич','Дмитриевич','Дорофеевич'];

        $key = array_rand($surnames);
        $surname = $surnames[$key];
        $key = array_rand($names);
        $name = $names[$key];
        $key = array_rand($patronymics);
        $patronymic = $patronymics[$key];
        
        return $surname.' '.$name.' '.$patronymic;
        
    }


    protected function generateDate() {
        
        $date = new DateTime();
        
        $current_year = (int) date('Y');
        
        $y = rand(1980, $current_year);
        
        $m = rand(1, 12);
        
        $d = rand(1, 30);
        
        return $date->setDate($y, $m, $d);
    }
}
