<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('employees/head/{id}', 'EmployeeController@head');

Route::get('employees', 'EmployeeController@index');

Route::get('employees/create', 'EmployeeController@create');

Route::post('employees/save', 'EmployeeController@save');

Route::get('employees/{id}/edit', 'EmployeeController@edit');

Route::put('employees/{id}/update', 'EmployeeController@update');

Route::delete('employees/{id}/delete', 'EmployeeController@delete');
    
Route::get('search', 'EmployeeController@search');

Route::controllers([
    '/' => 'WelcomeController'
]);

    

