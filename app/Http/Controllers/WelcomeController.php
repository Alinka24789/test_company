<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;

class WelcomeController extends Controller
{   
    public function getIndex(Request $request) {
        
        $employees = Employee::with('child', 'leader', 'position')
                ->orderBy('level', 'ASC')
                ->orderBy('fio', 'ASC')
                ->get();
        
        $employees_group = $employees->groupBy(function($item){
            return $item->level;
        });
        
        if($request->ajax()) {
            
            return response()->json($employees);
        }
        
        $general_employee = $employees_group[0][0];

        return view('main.index', [
            'general_employee' => $general_employee
        ]);
    }
}

