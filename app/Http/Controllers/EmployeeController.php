<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Position;

class EmployeeController extends Controller
{   
    
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
        
        $employees = Employee::with('child', 'leader', 'position')
                ->orderBy('level', 'ASC')
                ->orderBy('fio', 'ASC')
                ->get();
        
        return view('main.index_grid', [
                'employees' => $employees
            ]);
        
    }
    
    public function search(Request $request) {
        
        $employees = Employee::with('child', 'leader', 'position')
                ->orderBy('level', 'ASC')
                ->orderBy('fio', 'ASC')
                ->get();
        
        if($request->ajax()) {
            
            return response()->json($employees);
        }
        
        return view('main.index_grid', [
            'employees' => $employees
        ]);
    }
    
    public function head(Request $request, $id) {
        
        $employees = Employee::where('position_id', '=', $id)
                ->orderBy('fio', 'ASC')
                ->get();

        if ($request->ajax()) {

            if (!isset($employees)) {

                $employees = [];
            }

            return view('templates.employees_list', [
                'employees' => $employees
            ]);
        } else {
            return redirect('/');
        }
    }
    
    public function create() {
        
        $positions = Position::all()->toArray();
        
        $ids = array_column($positions, 'id');
        
        array_push($ids, '');
        
        $names = array_column($positions, 'name');
        
        array_push($names, 'Выберите должность');
        
        $result = array_combine($ids, $names);
        
        return view('main.create', [
            'positions' => $result
        ]);
                
    }
    
    public function save(Request $request) {
        
        $data = $request->input('employee');
        
        $v = Validator::make($data, [
           'fio' => 'required',
            'position_id' => 'required',
            'head' => 'required',
            'hire_date' => 'required|date',
            'salary' => 'required|min:0|numeric'
        ]);
        
        if($v->fails()) {
            
            return redirect()->back()->withErrors($v->errors())->withInput();
        }
        
        $head = Employee::find($data['head']);
        
        if(!isset($head->level)) {
            $level = 1;
        } else {
            $level = (int)$head->level + 1;
        }
        
        $employee = new Employee;
        
        $employee->fio = $data['fio'];
        $employee->position_id = $data['position_id'];
        $employee->head = $data['head'];
        $employee->salary = $data['salary'];
        $employee->level = $level;
        $employee->hire_date = $data['hire_date'];
        
        $employee->save();
        
        return redirect('/employees')->with('success', 'Сотрудник успешно зарегистрирован');
    }
    
    public function edit($id) {
        
        $positions = Position::all()->toArray();
        
        $ids = array_column($positions, 'id');
        
        array_push($ids, '');
        
        $names = array_column($positions, 'name');
        
        array_push($names, 'Выберите должность');
        
        $result = array_combine($ids, $names);
        
        $employee = Employee::with('leader', 'position')
                ->where('id', '=', $id)
                ->first();
        
        if(isset($employee->leader)) {
            $leaders = Employee::where('position_id', '=', $employee->leader->position_id)->get();
        } else {
            $leaders = [];
        }
        
        
        if(!isset($employee)) {
            
            return redirect('/employees');
        }
        
        return view('main.edit', [
            'positions' => $result,
            'employee' => $employee,
            'leaders' => $leaders
        ]);
                
    }
    
    public function update(Request $request, $id) {
        
        $data = $request->input('employee');
        
        $v = Validator::make($data, [
           'fio' => 'required',
            'position_id' => 'required',
            'head' => 'required',
            'hire_date' => 'required|date',
            'salary' => 'required|min:0|numeric'
        ]);
        
        if($v->fails()) {
            
            return redirect()->back()->withErrors($v->errors())->withInput();
        }
        
        $head = Employee::find($data['head']);
        
        if(!isset($head->level)) {
            $level = 1;
        } else {
            $level = (int)$head->level + 1;
        }
        
        $employee = Employee::find($id);
        
        if(!isset($employee)) {
            
            return redirect('/employees');
            
        }
        
        $employee->fio = $data['fio'];
        $employee->position_id = $data['position_id'];
        $employee->head = $data['head'];
        $employee->salary = $data['salary'];
        $employee->level = $level;
        $employee->hire_date = $data['hire_date'];
        
        $employee->save();
        
        return redirect('/employees')->with('success', 'Данные успешно изменены');
    }
    
    public function delete($id) {
        
        $employee = Employee::find($id);
        
        $employee->load('child');
        
        if(!isset($employee->head)) {
            
            return response()->json([
                'status' => 'error',
                'message' => 'Невозможно удалить сотрудника, так как он является главой компании.'
            ]);
        }
        
        $childs = Employee::where('head', '=', $employee->id)->get()->toArray();
        
        $ids = array_column($childs, 'id');
        
        DB::table('employees')
            ->whereIn('id', $ids)
            ->update(['head' => $employee->head, 'level' => $employee->level]);
        
        $employee->delete();
        
        return response()->json([
                'status' => 'ok',
                'message' => 'Сотрудник успешно удален'
            ]);
        
    }
    
}

