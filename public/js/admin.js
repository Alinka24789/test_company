/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    
    $('.datepicker-field').datepicker({
        dateFormat: "yy-mm-dd",
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
    });
    
    $('.chosen-select').chosen();
   
    var app = {
        AppSearchCollection: [],
        views: {},
        models: {},
        collections: {},
        func: {
            fetchAppCollection: function () {

                app.AppCollection = new app.collections.AppCollection;
                app.AppCollection.fetch();

            },
            fetchSearchCollection: function(data) {
                
                if(app.AppSearchCollection.length == 0) {
                    
                    app.AppSearchCollection = new app.collections.AppSearchCollection;
                    app.AppSearchCollection.fetch();
                    
                }
                
                app.func.initAppSearchCollection(data);
                
            },
            initAppSearchCollection: function(data) {
              
                var showResult = new app.views.ShowResult({
                   el: '#employee-list',
                   data: data
                });
            },
            initAppCollection: function (elem, level, head) {

                var showView = new app.views.ShowList({
                    el: elem,
                    level: level,
                    head: head
                });
            }
        }
    }
    
        
    app.models.AppModal = Backbone.Model.extend({
        idAttribute: 'id'
    });
    
    app.collections.AppCollection = Backbone.Collection.extend({
        model: app.models.AppModal,
        url: '/'
    });
    
    app.collections.AppSearchCollection = Backbone.Collection.extend({
        model: app.models.AppModal,
        url: '/search'
    });
    
    
    app.views.ShowList = Backbone.View.extend({
         initialize: function (options) {
             
            this.listenTo(app.AppCollection, 'sync', this.render);
            
            this.level = options.level;
            
            this.head = options.head;
            
            if (app.AppCollection.length !== 0) {
                return this.render();
            }
            return this;
        },
        render: function() {
            
            var that = this;
            
            var collection = app.AppCollection;
            
            this.$el.append('<ul class="list-group"></ul>');
            
            this.$el.find('button').html('<i class="fa fa-minus"></i>').addClass('active');
            
            var list = collection.filter(function(model){
                 return model.get('head') == that.head;    
            });
            
            if(list.length == 0) {
                that.$el.find('ul').append('<li class="list-group-item">Сотрудники отсутствуют</li>');
            }
                _.each(list, function(model){
                    
                        var showItem = new app.views.ShowItem({
                            model: model,
                            collection: collection,
                        });
                        
                        that.$el.find('ul').append(showItem.el);
                });
                
            
            if(collection.length == 0) {
                this.$el.append('Ничего не найдено!');
            }
            
            return this;
        }
    });
    
    app.views.ShowItem = Backbone.View.extend({
        tagName: 'li',
        className: 'list-group-item',
        initialize: function (options) {
             
            this.template = _.template($('#template-item-employee').html());
            return this.render();
            
        },
        render: function () {
            
            this.$el.html(this.template({model: this.model.toJSON()}));
            return this;
            
        }
    });
    
    app.views.ShowResult = Backbone.View.extend({
        initialize: function (options) {
             
            this.listenTo(app.AppSearchCollection, 'sync', this.render);
            
            this.data = options.data;
            
            if (app.AppSearchCollection.length !== 0) {
                return this.render();
            }
            
            return this;
        },
        render: function() {
            
            var that = this;
            
            that.$el.empty();
            
            var collection = app.AppSearchCollection;
            
            var result_collection = search_filter(collection, this.data);
            
            var i = 0;
            
            _.each(result_collection, function(model) {
                
                if(i == 4) {
                    
                    that.$el.append("<div style='display: block; width: 100%; float: left;'></div>");
                    
                    i = 0;
                }
                
                i++;
               
                var showItemResult = new app.views.ShowItemResult({
                            model: model,
                            collection: collection,
                        });
                        
                that.$el.append(showItemResult.el);
            });
            
            return this;
        }
    });
    
    app.views.ShowItemResult = Backbone.View.extend({
        tagName: 'div',
        className: 'col-md-3',
        initialize: function () {
             
            this.template = _.template($('#template-item-search-employee').html());
            return this.render();
            
        },
        render: function () {
            
            var result_model = this.model.toJSON();
            
            this.$el.html(this.template({model: this.model.toJSON()}));
            return this;
            
        }
    });
    
    $('body').on('click', '[data-id="show-list"]', function(){
        
        if ($(this).hasClass('active')) {

            $(this).closest('li').find('ul').detach();

            $(this).html('<i class="fa fa-plus"></i>');

            $(this).removeClass('active');
            
        } else {

            var level = $(this).data('toggle');
            
            var head = $(this).data('head');

            var parents = $(this).closest('li');

            app.func.initAppCollection(parents, level, head);

        }
        
    });
    
    
    $('#form-sort').on('submit', function(){
       
        var fields = $(this).find('input');
        
        var that = this;
        
        var data = {};
        
        $('#reset-filter').removeClass('hide');
        
        _.each(fields, function (value, key) {
            data[$(value).attr('name')] = $(value).val();
        });
        
        app.func.fetchSearchCollection(data);
        
        return false;
        
    });
    
    $('#employee-position_head').on('change', function(){
        
        var position = $(this).val();
        
        $.ajax({
           url: '/employees/head/'+position,
           method: 'get',
            beforeSend: function (xhr) {
                initToken(xhr);
            },
            success: function(data) {
                
                $('#employee-head').html(data);
                
                $('#employee-head').trigger("chosen:updated");
            }
        });
        
    });
    
    $('body').on('click', "[data-id='delete-employee']", function(){
       
        var accept = confirm("Вы действительно хотите удалить сотрудника?");
        
        var href = $(this).data('href');
        
        if(accept) {
            
            $.ajax({
                url: href,
                method: 'delete',
                dataType: 'json',
                beforeSend: function (xhr) {
                    initToken(xhr);
                }, 
                success: function(data) {
                    alert(data.message);
                    location.reload();
                }
            });
            
        }
    });
    
    app.func.fetchAppCollection();
    
});


function search_filter(collection, data) {
    
    if(data.fio !== '') {
        
        collection = collection.filter(function (model) {
                return (((model.get('fio')).toLowerCase()).indexOf((data.fio).toLowerCase()) + 1);
            });
    
    }
    
    if(data.position !== '') {
        
        collection = collection.filter(function (model) {
                return (((model.get('position').name).toLowerCase()).indexOf((data.position).toLowerCase()) + 1);
            });
        
    }
    
    if(data.salary_min !== '') {
        
        collection = collection.filter(function (model) {
                return model.get('salary') >= parseInt(data.salary_min);
            });
    }
    
    if(data.salary_max !== '') {
        
        collection = collection.filter(function (model) {
                return model.get('salary') <= parseInt(data.salary_max);
            });
    }
    
    if (data.head !== '') {

        collection = collection.filter(function (model) {
            if (model.get('leader') !== null) {
                return (((model.get('leader').fio).toLowerCase()).indexOf((data.head).toLowerCase()) + 1);
            }
        });

    }
    
    if(data.hire_date_at !== '') {
        
        collection = collection.filter(function (model) {
                var d1 = new Date(model.get('hire_date'));
                var d2 = new Date(data.hire_date_at);
                return d1 >= d2;
            });
    }
    
    if(data.hire_date_to !== '') {
        
        collection = collection.filter(function (model) {
                var d1 = new Date(model.get('hire_date'));
                var d2 = new Date(data.hire_date_to);
                return d1 <= d2;
            });
    }
    
    return collection;
}

function initToken(request) {
    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
}