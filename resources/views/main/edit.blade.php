@extends('app')

@section('content')

<div class="col-md-offset-2 col-md-8">
    <h2>Изменение данных о сотруднике</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Ошибка!</strong> Проверьте правильность ввода данных.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    {!! Form::open(array('url' => 'employees/'.$employee->id.'/update', 'method' => 'put')) !!}
    <div class='form-group'>
        <label for='employee-fio'>ФИО</label>
        {!! Form::text('employee[fio]', $employee->fio, array('class' => 'form-control', 'id' => 'employee-fio'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-position_id'>Должность</label>
        {!! Form::select('employee[position_id]', $positions, $employee->position_id, array('class' => 'form-control chosen-select', 'id' => 'employee-position_id'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-position_head'>Должность руководителя</label>
        @if(isset($employee->leader))
        {!! Form::select('position_head', $positions, $employee->leader->position_id, array('class' => 'form-control chosen-select', 'id' => 'employee-position_head'))!!}
        @else
        {!! Form::select('position_head', $positions, '', array('class' => 'form-control chosen-select', 'id' => 'employee-position_head'))!!}
        @endif
    </div>
    <div class='form-group'>
        <label for='employee-head'>Выбурите руководителя</label>
        <select name='employee[head]' class='form-control chosen-select' id='employee-head'>
            <option value=''>Выберите руководителя</option>
            @foreach($leaders as $leader)
            <option value='{{$leader->id}}' @if($leader->id == $employee->head) selected="selected" @endif>{{$leader->fio}}</option>
            @endforeach
        </select>
    </div>
    <div class='form-group'>
        <label for='employee-hire_date'>Дата приема на работу</label>
        {!! Form::text('employee[hire_date]', $employee->hire_date, array('class' => 'form-control datepicker-field', 'id' => 'employee-hire_date'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-salary'>Оклад</label>
        {!! Form::text('employee[salary]', $employee->salary, array('class' => 'form-control', 'id' => 'employee-salary'))!!}
    </div>
    <div class='form-group'>
        {!! Form::submit('Сохранить изменения', array('class' => 'btn btn-primary'))!!}
        <a type='button' class='btn btn-danger' href='/employees'>Отмена</a>
    </div>
    {!! Form::close() !!}
</div>

@endsection
