@extends('app')

@section('content')

<div class="col-md-offset-2 col-md-8">
    <h2>Регистрация нового сотрудника</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Ошибка!</strong> Проверьте правильность ввода данных.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    {!! Form::open(array('url' => 'employees/save', 'method' => 'post')) !!}
    <div class='form-group'>
        <label for='employee-fio'>ФИО</label>
        {!! Form::text('employee[fio]', '', array('class' => 'form-control', 'id' => 'employee-fio'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-position_id'>Должность</label>
        {!! Form::select('employee[position_id]', $positions, '', array('class' => 'form-control chosen-select', 'id' => 'employee-position_id'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-position_head'>Должность руководителя</label>
        {!! Form::select('position_head', $positions, '', array('class' => 'form-control chosen-select', 'id' => 'employee-position_head'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-head'>Выбурите руководителя</label>
        <select name='employee[head]' class='form-control chosen-select' id='employee-head'>
            
        </select>
    </div>
    <div class='form-group'>
        <label for='employee-hire_date'>Дата приема на работу</label>
        {!! Form::text('employee[hire_date]', '', array('class' => 'form-control datepicker-field', 'id' => 'employee-hire_date'))!!}
    </div>
    <div class='form-group'>
        <label for='employee-salary'>Оклад</label>
        {!! Form::text('employee[salary]', '', array('class' => 'form-control', 'id' => 'employee-salary'))!!}
    </div>
    <div class='form-group'>
        {!! Form::submit('Зарегистрировать', array('class' => 'btn btn-primary'))!!}
        <a type='button' class='btn btn-danger' href='/employees'>Отмена</a>
    </div>
    {!! Form::close() !!}
</div>

@endsection
