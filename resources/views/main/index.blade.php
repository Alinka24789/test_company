@extends('app')

@section('content')
<div class="col-lg-12">
    <a class="btn btn-primary" href="/">
        <i class="fa fa-th-list"></i>
    </a>
    <a class="btn btn-link" href="/employees">
        <i class="fa fa-th-large"></i>
    </a>
</div>
<div class="col-lg-12">
    <ul class="list-group">
        <li class="list-group-item"> 
            <h4>{{$general_employee->position->name}}</h4>
            <h3>{{$general_employee->fio}}</h3> 
            <ul class="list-group">
                @foreach($general_employee->child as $employee)
                <li class="list-group-item">
                    <h5>{{$employee->position->name}}</h5>
                    <h4>
                        <button class="btn btn-link" data-id="show-list" data-toggle="{{$employee->level}}" data-head="{{$employee->id}}"><i class="fa fa-plus"></i></button>
                        {{$employee->fio}}
                    </h4> 

                </li>
                @endforeach
            </ul>
        </li>
    </ul>
</div>

@endsection



<script type="text/html" id="template-item-employee">
    <h5><%= model.position.name %></h5>
    <h4>
        <button class="btn btn-link" data-id="show-list" data-toggle="<%= model.level %>" data-head="<%= model.id %>"><i class="fa fa-plus"></i></button>
        <%= model.fio %>
    </h4> 
</script>