@extends('app')

@section('content')
<div class="col-lg-12">
    <a class="btn btn-link" href="/">
        <i class="fa fa-th-list"></i>
    </a>
    <a class="btn btn-primary" href="/employees">
        <i class="fa fa-th-large"></i>
    </a>
    <a class="btn btn-primary" href="/employees/create">
        Добавить сотрудника
    </a>
    <a class="btn btn-link hide" id='reset-filter' href="/employees">
        Сбросить фильтр
    </a>
</div>
<div class="col-lg-12">
    @include('messages')
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form" method="get" action="/" id="form-sort">
                <div class="col-md-2">
                    <input type="text" name="fio" placeholder="ФИО" class="form-control">
                </div>
                <div class="col-md-2">
                    <input type="text" name="position" placeholder="Должность" class="form-control">
                </div>
                <div class="col-md-2">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <input type="text" name="salary_min" placeholder="Cумма от" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <input type="text" name="salary_max" placeholder="Сумма до" class="form-control">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <input type="text" name="head" placeholder="Руководитель" class="form-control">
                </div>
                <div class="col-md-3">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                        <input type="text" name="hire_date_at" placeholder="Дата приема от" class="form-control datepicker-field">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                        <input type="text" name="hire_date_to" placeholder="Дата приема до" class="form-control datepicker-field">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-1">
                <button type="submit" class="btn btn-primary" id="sort-employees">
                    <i class="fa fa-search"></i>
                </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="row" id="employee-list">
        <?php $line = 0; ?>
        @foreach($employees as $employee)
        <?php $line++; 
            if($line == 1) :
       ?>
        <div class="col-md-12">
        <?php
            endif;
        ?>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5>{{$employee->fio}}</h5>
                </div>
                <div class="panel-body">
                    <p><strong>Должность: </strong> {{$employee->position->name}}</p>
                    @if(isset($employee->leader))
                    <p>Руководитель: {{$employee->leader->fio}}</p>
                    @endif
                    <p>Принять на работу: {{$employee->hire_date}}</p>
                    <p>Зарплата: {{$employee->salary}} руб.</p>
                </div>
                <div class='panel-footer'>
                    <a href='/employees/{{$employee->id}}/edit' class='btn btn-link'><i class='fa fa-edit'></i></a>
                    <button data-href='/employees/{{$employee->id}}/delete' class='btn btn-link' data-id='delete-employee'><i class='fa fa-trash'></i></button>
                </div>
            </div>
        </div>
         <?php if($line == 4) : ?>
            </div>
        <?php $line = 0;  endif; ?>
        @endforeach
    </div>
</div>

@endsection




<script type="text/html" id="template-item-search-employee">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h5><%= model.fio %></h5>
            </div>
            <div class="panel-body">
                <p><strong>Должность: </strong> <%= model.position.name %></p>
                <% if(model.leader !== null) { %>
                <p>Руководитель: <%= model.leader.fio %></p>
                <% } %>
                <p>Принять на работу: <%= model.hire_date %></p>
                <p>Зарплата: <%= model.salary %> руб.</p>
            </div>
            <div class='panel-footer'>
                    <a href='/employees/<%= model.id %>/edit' class='btn btn-link'><i class='fa fa-edit'></i></a>
                    <button href='/employees/<%= model.id %>/delete' class='btn btn-link' data-id='delete-employee'><i class='fa fa-trash'></i></button>
                </div>
        </div>
</script>