<!DOCTYPE html>
<html>
    <head>
        <title>Компания</title>
        
        <meta name='csrf-token' content="{{csrf_token()}}">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        <link href="/fonts/awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

        <!-- jQuery Datepicker style-->
        <link href='/library/datepicker/jquery-ui.min.css' rel='stylesheet' type='text/css'>
        <link href='/library/datepicker/jquery-ui.theme.min.css' rel='stylesheet' type='text/css'>

        <!-- Fancybox style-->
        <link href='/library/fancybox/jquery.fancybox.css' rel='stylesheet' type='text/css'>

        <!-- Chosen style-->
        <link href='/library/chosen/chosen.min.css' rel='stylesheet' type='text/css'>

        <link href="/css/app.css" rel="stylesheet" type="text/css">
        
    </head>
    <body>
        <nav class="navbar navbar-inverse">
                <div class="container">

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Вход</a></li>
                        @else
                        <li>
                            <a href="{{ url('/auth/logout') }}" title="Выйти">Выход</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        <div class="container">
            
            @yield('content')
        </div>
    </body>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/library/bootstrap/javascripts/bootstrap.min.js"></script>
    
    <!--jQuery maskedinput script-->
    <script src='/library/maskedinput/jquery.maskedinput.min.js'></script>
    
    <!--Datepicker script-->
    <script src="/library/datepicker/jquery-ui.min.js"></script>
    
    <!--Backbone && Undescore-->
    <script src='/library/backbone/underscore.js'></script>
    <script src='/library/backbone/backbone.js'></script>
    
    <!--Fancybox script-->
    <script src="/library/fancybox/jquery.fancybox.pack.js"></script>
    
    <!--Chosen script-->
    <script src="/library/chosen/chosen.jquery.js"></script>
    
    <script src="/js/admin.js"></script>
    
</html>

